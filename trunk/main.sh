#! /bin/bash

mkdir data
rm data/chrono*
rm data/error*

cp order.hpp_template order.hpp
sed -i 's/order = 1/order = 3/g' order.hpp
sed -i 's/rkOrder = 1/rkOrder = 3/g' order.hpp
make

for (( n=10; n<=4000; n*=2))
do
    echo $n
    ./main $n 0.167
done

./main 353 0.167

cp order.hpp_template order.hpp
sed -i 's/order = 1/order = 4/g' order.hpp
sed -i 's/rkOrder = 1/rkOrder = 4/g' order.hpp
make

for (( n=10; n<=1000; n*=2))
do
    echo $n
    ./main $n 0.116
done

./main 132 0.132

cp order.hpp_template order.hpp
sed -i 's/order = 1/order = 5/g' order.hpp
sed -i 's/rkOrder = 1/rkOrder = 5/g' order.hpp
make

for (( n=10; n<=800; n*=2))
do
    echo $n
    ./main $n 0.092
done

./main 75 0.092

cp order.hpp_template order.hpp
sed -i 's/order = 1/order = 6/g' order.hpp
sed -i 's/rkOrder = 1/rkOrder = 6/g' order.hpp
make

for (( n=10; n<=500; n*=2))
do
    echo $n
    ./main $n 0.0744
done

./main 51 0.0744

# # error 0.1
# #./main 1300 0.8

# cp main.cpp_template main.cpp
# sed -i 's/order = 1/order = 2/g' main.cpp
# sed -i 's/rkOrder = 1/rkOrder = 2/g' main.cpp
# g++ -O3 -std=c++11 -o main main.cpp

# for (( n=10; n<=10000; n*=2))
# do
#     echo $n
#     ./main $n 0.2664
# done

# # error 0.1
# #./main 95 0.2664

# # error 0.01
# #./main 273 0.2664

# cp main.cpp_template main.cpp
# sed -i 's/order = 1/order = 3/g' main.cpp
# sed -i 's/rkOrder = 1/rkOrder = 3/g' main.cpp
# g++ -O3 -std=c++11 -o main main.cpp

# for (( n=10; n<=2000; n*=2))
# do
#     echo $n
#     ./main $n 0.167
# done

# # error 0.1
# #./main 27 0.167

# # error 0.01
# #./main 45 0.167

# cp main.cpp_template main.cpp
# sed -i 's/order = 1/order = 4/g' main.cpp
# sed -i 's/rkOrder = 1/rkOrder = 4/g' main.cpp
# g++ -O3 -std=c++11 -o main main.cpp

# for (( n=10; n<=600; n*=2))
# do
#     echo $n
#     ./main $n 0.116
# done

# # error 0.1
# #./main 14 0.116

# # error 0.01
# #./main 20 0.116

# cp main.cpp_template main.cpp
# sed -i 's/order = 1/order = 4/g' main.cpp
# sed -i 's/rkOrder = 1/rkOrder = 3/g' main.cpp
# g++ -O3 -std=c++11 -o main main.cpp

# for (( n=10; n<=1000; n*=2))
# do
#     echo $n
#     ./main $n 0.104
# done

# # error 0.1
# #./main 16 0.104

# # error 0.01
# #./main 26 0.104

# cp main.cpp_template main.cpp
# sed -i 's/order = 1/order = 5/g' main.cpp
# sed -i 's/rkOrder = 1/rkOrder = 5/g' main.cpp
# g++ -O3 -std=c++11 -o main main.cpp

# for (( n=10; n<=400; n*=2))
# do
#     echo $n
#     ./main $n 0.092
# done

# # error 0.1
# #./main 16 0.104

# # error 0.01
# #./main 26 0.104

# cp main.cpp_template main.cpp
# sed -i 's/order = 1/order = 6/g' main.cpp
# sed -i 's/rkOrder = 1/rkOrder = 6/g' main.cpp
# g++ -O3 -std=c++11 -o main main.cpp

# for (( n=10; n<=400; n*=2))
# do
#     echo $n
#     ./main $n 0.0744
# done

# # error 0.1
# #./main 16 0.104

# # error 0.01
# #./main 26 0.104
