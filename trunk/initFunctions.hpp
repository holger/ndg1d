#ifndef initFunctions_hpp
#define initFunctions_hpp

class InitFunction
{
public:
  InitFunction(double lambda, double shift = 0) : lambda_(lambda), shift_(shift) {}
  
  double operator()(double x) const{
    //    double constant = 1;
    // double k = 1;
    // double temp = sin(2*M_PI*k/lambda_*(x-shift_));
    
    int kMax = 40;
    double temp = 0;
    for(int k=1; k<=kMax; k++)
      temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift_-2*M_PI/(k*k)));

    // double temp = constant;
    // if(x > 0.4 && x < 0.6)
    //   temp = constant + 1;
    
    // // if(x > 0.25 && x < 0.25+lambda_/2)
    // //   temp = constant + sin(2*M_PI/lambda_*((x-0.25)));
    // double temp = constant;
    // if(x > 0.7 && x < 0.9)
    //   temp = constant + 1;
    
    // if(x > 0.1 && x < 0.1+lambda_/2)
    //   temp = constant + sin(2*M_PI/lambda_*((x-0.1)));
    
    return temp;
  }
  
private:
  double lambda_;
  double shift_;
  
};

#endif
