
//==================================================================
/**
 *  legendre.h -- C++ functions to evaluate Legendre polynomials
 *
 *  Copyright (C) 2005 by James A. Chappell
 *
 *  Permission is hereby granted, free of charge, to any person
 *  obtaining a copy of this software and associated documentation
 *  files (the "Software"), to deal in the Software without
 *  restriction, including without limitation the rights to use,
 *  copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following
 *  condition:
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *  OTHER DEALINGS IN THE SOFTWARE.
 */
//=================================================================
/*
 * legendre.h:  Version 0.01
 * Created by James A. Chappell
 * Created 29 September 2005
 *
 * History:
 * 29-sep-2005  created
 */
//==============

#ifndef __LEGENDRE_H__
#define __LEGENDRE_H__
/*
 *	Function calculates Legendre Polynomials Pn(x)
 */

#include <cassert>

namespace Legendre
{
  // n = 0
  inline double P0(double x)
  {
    return 1.0 ;
  }

  // n = 1
  inline double P1(double x)
  {
    return x ;
  }

  // n = 2
  inline double P2(double x)
  {
    return ((3.0 * x*x) - 1.0) * 0.5 ;
  } 

  // n = 3
  inline double P3(double x)
  {
    return 0.5*(5*x*x*x - 3*x) ;
  }

  // n = 4
  inline double P4(double x)
  {
    return (35*x*x*x*x - 30*x*x + 3)/8. ;
  }
  
  // n = 5
  inline double P5(double x)
  {
    return (63*x*x*x*x*x - 70*x*x*x + 15*x)/8. ;
  } 

/*
 *	Pn(x)
 */
  inline double Pn(unsigned int n, double x)
  {
    if (n == 0)
    {
      return P0(x) ;
    }
    else if (n == 1)
    {
      return P1(x) ;
    }
    else if (n == 2)
    {
      return P2(x) ;
    }
    else if (n == 3)
    {
      return P3(x) ;
    }
    else if (n == 4)
    {
      return P4(x) ;
    }
    else if (n == 5)
    {
      return P5(x) ;
    }
    
    if (x == 1.0)
    {
      return 1.0 ;
    }

    if (x == -1.0)
    {
      return ((n % 2 == 0) ? 1.0 : -1.0) ;
    }

    if ((x == 0.0) && (n % 2))
    {
      return 0.0 ;
    }

/* We could simply do this:
    return (double(((2 * n) - 1)) * x * Pn(n - 1, x) -
          (double(n - 1)) * Pn(n - 2, x)) / (double)n ;
   but it could be slow for large n */
  
    double pnm1(P2(x)) ;
    double pnm2(P1(x)) ;
    double pn(pnm1) ;

    for (unsigned int l = 3 ; l <= n ; l++)
    { 
      pn = (((2.0 * (double)l) - 1.0) * x * pnm1 - 
            (((double)l - 1.0) * pnm2)) / (double)l ;
      pnm2 = pnm1;
      pnm1 = pn;
    }

    return pn ;
  }

  // n = 0
  inline double DP0(double x)
  {
    return 0 ;
  }

  // n = 1
  inline double DP1(double x)
  {
    return 1. ;
  }

  // n = 2
  inline double DP2(double x)
  {
    return 3*x ;
  }

  // n = 3
  inline double DP3(double x)
  {
    return 0.5*(-3+15*x*x) ;
  }

  // n = 4
  inline double DP4(double x)
  {
    return (-60*x + 140*x*x*x)/8. ;
  }

  // n = 5
  inline double DP5(double x)
  {
    return (315*x*x*x*x - 210*x*x + 15)/8. ;
  }

  inline double DPn(int n, double x)
  {
    assert(n < 6 && n > -1);
      
    if (n == 0)
    {
      return DP0(x) ;
    }
    else if (n == 1)
    {
      return DP1(x) ;
    }
    else if (n == 2)
    {
      return DP2(x) ;
    }
    else if (n == 3)
    {
      return DP3(x) ;
    }
    else if (n == 4)
    {
      return DP4(x) ;
    }
    else if (n == 5)
    {
      return DP5(x) ;
    }

    else {
      std::cerr << "ERROR in DPn: n > 5\n";
      exit(1);
      return 0;
    }
  }   
} // namespace Legendre
#endif
