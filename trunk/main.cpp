// cp main.cpp_template main.cpp; g++ -O3 -std=c++11 -o main main.cpp

#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <array>
#include <sstream>
#include <string>
#include <chrono>
#include <ctime>

#include "util.hpp"
#include "dgData.hpp"
#include "initFunctions.hpp"
#include "rk.hpp"

int nInt = 100;

void computeError(const BK::Vector<double>& ref, const BK::Vector<double> vec, const std::string& fileName, double dx) {
  std::ofstream out(fileName.c_str(), std::ios::app);

  double error = 0;
  for(size_t i=0;i<ref.size(); i++)
    error += sqr(ref[i] - vec[i]);
  
  error *= dx;
  error = sqrt(error);

  out << ref.size()/nInt << "\t" << error << std::endl;
}

int nx;
double dx;
double lambda;

//const int order = 6;
//const int rkOrder = 6;

typedef DgData<double,order> Dg;

int main(int argc, char** argv)
 {
  std::cerr << "Test of the discontinious Galerkin method for 1d advection starts ...\n";

  if(argc < 3) {
    std::cerr << "parameters : nx cfl\n";
    exit(1);
  }

  nx = atoi(argv[1]);
  double cfl = atof(argv[2]);

  lambda = 1;
  dx = lambda/(nx-2);
  double lx = lambda + 2*dx;

  //  double dInt = 2./(nInt-1);
  double ddx = dx/(nInt);
  double a = 1;
  double dt = cfl*dx/fabs(a);

  double nStepsD = lambda/a/dt;
  int nSteps = 1*round(lambda/a/dt);
  //  nSteps = 10;
  
  double deltaNSteps = nSteps - nStepsD;

  std::cout << deltaNSteps << "\t" << nSteps << "\t" << nStepsD << std::endl;
  
  //int nSteps = 10;
  std::cout << "nSteps = " << nSteps << std::endl;
  std::cout << "cfl = " << cfl << std::endl;
  std::cout << "dx = " << dx << std::endl;
  std::cout << "dt = " << dt << std::endl;
  std::cout << "rkOrder = " << rkOrder << std::endl;

  // std::ofstream out("lagrange.txt");
  // double dx = 2./(nx-1);
  // BK::Vector<double> lagrangeVec0(nx);
  // BK::Vector<double> lagrangeVec1(nx);
  // BK::Vector<double> lagrangeVec2(nx);
  // for(int i=0;i<nx;i++) {
  //   lagrangeVec0[i] = lagrange(0,3,-1+i*dx);
  //   lagrangeVec1[i] = lagrange(1,3,-1+i*dx);
  //   lagrangeVec2[i] = lagrange(2,3,-1+i*dx);
  //   out << -1 + i*dx << "\t" << lagrangeVec0[i] << "\t" << lagrangeVec1[i] << "\t" << lagrangeVec2[i] << std::endl;
  // }
  // out.close();
  
  // out.open("dlagrange.txt");
  // for(int i=0;i<nx;i++) {
  //   lagrangeVec0[i] = dLagrange(0,3,-1+i*dx);
  //   lagrangeVec1[i] = dLagrange(1,3,-1+i*dx);
  //   lagrangeVec2[i] = dLagrange(2,3,-1+i*dx);
  //   out << -1 + i*dx << "\t" << lagrangeVec0[i] << "\t" << lagrangeVec1[i] << "\t" << lagrangeVec2[i] << std::endl;
  // }
  // out.close();
  
  Dg dg(nx, lx);
  Dg dgNew(nx, lx);

  BK::Vector<double> uNew(nx);
  BK::Vector<double> uFineNew(nx*nInt);

  BK::Vector<double> uRef(nx);
  BK::Vector<double> uRefFine(nx*nInt);

  InitFunction initFunction(lambda);

  dg.initFromFunction(initFunction);

  dg.writeFunctionFine(toString("data/uFineInit_",order,"-",rkOrder,"_",nx,"_",cfl,".txt"));
  
  dg.writeFunction(toString("data/uInit_",order,"-",rkOrder,"_",nx,"_",cfl,".txt"));

  //  dg.writeFunctionFine(toString("data/uInitFine_",order,"-",rkOrder,"_",nx,"_",cfl,".txt"));
  
  auto start = std::chrono::system_clock::now();

  for(int iter = 0;iter<nSteps;iter++) {
    //    std::cout << "step = " << iter << std::endl;

    if(rkOrder == 1)
      singleStepEuler(dg, dgNew, dt, dx, a);

    if(rkOrder == 2)
      singleStepRk2(dg, dgNew, dt, dx, a);

    if(rkOrder == 3)
      singleStepRk3(dg, dgNew, dt, dx, a);
    
    if(rkOrder == 4)
      singleStepRk4(dg, dgNew, dt, dx, a);

    if(rkOrder == 5)
      singleStepRk5(dg, dgNew, dt, dx, a);
    
    if(rkOrder == 6)
      singleStepRk6(dg, dgNew, dt, dx, a);
    
    dg = dgNew;
  }

  auto end = std::chrono::system_clock::now();
  
  std::chrono::duration<double> elapsed_seconds = end-start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  
  std::cout << "finished computation at " << std::ctime(&end_time)
	    << "elapsed time: " << elapsed_seconds.count() << "s\n";

  std::string fileName = toString("data/chronoDG-",order,"-",rkOrder,".txt");
  std::ofstream out(fileName.c_str(), std::ios::app);
  out << nx << "\t" << elapsed_seconds.count() << std::endl;
  out.close();
  
  dg.getFunction(uNew);
  dg.writeFunction(toString("data/uNew_",order,"-",rkOrder,"_",nx,"_",cfl,".txt"));

  dg.getFunctionFine(uFineNew);  
  dg.writeFunctionFine(toString("data/uFineNew_",order,"-",rkOrder,"_",nx,"_",cfl,".txt"));

  double shift = nSteps*dt*a;
  InitFunction initFunctionRef(lambda, shift);
  dg.initFromFunction(initFunctionRef);
  
  dg.getFunction(uRef);
  dg.writeFunction(toString("data/uRef_",order,"-",rkOrder,"_",nx,"_",cfl,".txt"));
  
  dg.getFunctionFine(uRefFine);
  dg.writeFunctionFine(toString("data/uFineRef_",order,"-",rkOrder,"_",nx,"_",cfl,".txt"));
  
  fileName = toString("data/errorDG-",order,"-",rkOrder,".txt");
  computeError(uRefFine, uFineNew, fileName, ddx);
}
