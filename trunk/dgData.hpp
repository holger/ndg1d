#ifndef dgData_h
#define dgData_h

#include <vector>
#include <array>
#include <cassert>

#include <BasisKlassen/assert.hpp>

#include "gaussLobatto.hpp"
#include "lagrange.hpp"

template<typename T, int Order>
class DgData
{
public:
  typedef T type;
  static const int ORDER = Order;
  
  DgData(int size, double lx) {
    size_ = size;
    lx_ = lx;
    dx_ = lx/(size);
    //    lambda_ = lx_dx_;
    order_ = ORDER;
    
    static bool infosWritten = false;

    if(!infosWritten) {
      std::cout << "DG infos:\n";
      std::cout << "order_ = " << order_ << std::endl;
      std::cout << "size_ = " << size_ << std::endl;
      infosWritten = true;
    }    

    if(size != 0)
      data_.resize(size);
  }
  
  T& operator()(int cell, int point) {
    ASSERT(cell >= 0 && cell < size_, BK::toString("cell = ",cell,", size_ = ", size_));
    ASSERT(point >= 0 && point < order_, BK::toString("point = ",point,", order_ = ", order_));

    return data_[cell][point];
  }
  
  T operator()(int cell, int point) const {
    ASSERT(cell >= 0 && cell < size_, BK::toString("cell = ",cell,", size_ = ", size_));
    ASSERT(point >= 0 && point < order_, BK::toString("point = ",point,", order_ = ", order_));

    return data_[cell][point];
  }

  DgData& operator=(DgData const& dgData) {
    order_ = dgData.order_;
    size_ = dgData.size_;
    lx_ = dgData.lx_;
    dx_ = dgData.dx_;
    //    lambda_ = dgData.lambda_;
    
    for(int i=0;i<dgData.size();i++)
      data_[i] = dgData.data_[i];

    return *this;
  }

  int size() const {
    return size_;
  }

  int order() const {
    return order_;
  }

  double dx() const {
    return dx_;
  }

  T lx() const {
    return lx_;
  }

  template<class Functor>
  void initFromFunction(Functor const& function) 
  {
    std::cerr << "void initFromFunction(Functor const& function)\n";
    
    static BK::Vector<double> xj = points[ORDER-3];
    static BK::Vector<double> wi = weights[ORDER-3];

    assert(int(xj.size()) == order_);
    
    for(int cell=0;cell<size_;cell++)

      for(int point=0; point < order_; point++) {
	data_[cell][point] = function(cell*dx_ + xj[point]*dx_/2 + dx_/2);
	//	std::cerr << cell << "\t" << point << "\t" << data_[cell][point] << std::endl;
      }
    
    std::cerr << "void initFromFunction(Functor const& function)-end\n"; 
  }

  void writeFunction(std::string const& filename)
  {
    BK::Vector<T> vec;
    getFunction(vec);
    
    std::ofstream out(filename.c_str());
    for(int i=0;i<size_;i++) 
      out << i*dx_ + dx_/2 << "\t" << vec[i] << std::endl;
    
    out.close();
  }

  void writeFunctionFine(std::string const& filename)
  {
    BK::Vector<T> vec;
    getFunctionFine(vec);

    //    double dInt = 2./(nInt_-1);
    double ddx = dx_/(nInt_-1);
    
    std::ofstream out(filename.c_str());
    for(int cell=0;cell<size_;cell++)
      for(int j=0;j<nInt_;j++)
	out << cell*dx_ + j*ddx << "\t" << vec[cell*nInt_+j] << std::endl;
 
    out.close();
  }

  void getFunction(BK::Vector<T>& vec)
  {
    vec.resize(size_);
    for(size_t i=0;i<vec.size();i++)
      vec[i] = 0;

    for(size_t cell=0; cell<vec.size();cell++) {
      vec[cell] = getValue(cell,0);
      //      std::cerr << "value = " << vec[cell] << "\t" << cell << std::endl; 
    }
  }

  T getValue(int cell, double x) const {
    T value = 0;
    for(int order=0;order<order_;order++) {
      value += data_[cell][order]*lagrange(order, order_, x);
      //      std::cerr << "value = " << value << "\t" << order << "\t" << data_[cell][order] << "\t" << cell << "\t" << lagrange(order, order_, x) << std::endl;
    }
    //   
    
    return value;
  }

  void getFunctionFine(BK::Vector<T>& vec)
  {
    double dInt = 2./(nInt_-1);

    vec.resize(size_*nInt_);
    vec = 0.;
    
    for(int cell=0;cell<size_;cell++)
      for(int j=0;j<nInt_;j++)
	vec[cell*nInt_+j] += getValue(cell, -1+j*dInt);
  }
  
private:
  BK::Vector<BK::Array<double, ORDER>> data_;
  
  int size_;
  T lx_;
  double dx_;
  //  double lambda_;
  int nInt_ = 100;
  int order_;


};

template<class DgData>
void periodic(DgData& coeff)
{
  for(int order=0; order<coeff.order(); order++) {
    coeff(0, order) = coeff(coeff.size()-2,order);
    coeff(coeff.size()-1,order) = coeff(1,order);
  }
}

template<class DgData>
void dg(const DgData& dgData, DgData& rhs,
	 double dt, double dx, double a)
{
  BK::Vector<double> fin(dgData.size());
  BK::Vector<double> fout(dgData.size());

  for(int i=1;i<dgData.size()-1;i++) {
    fin[i] = a*dgData(i-1,order-1);
    fout[i] = a*dgData(i,order-1);
  }

  static BK::Vector<double> xj = points[order-3];
  static BK::Vector<double> wi = weights[order-3];

  static DLagrangeMatrix<order> dLagrangeMatrix;
  
  for(int cell=1;cell<dgData.size()-1;cell++) {
    for(int k=0; k<dgData.order();k++) {
      double value = 0;
      for(int point=0; point<dgData.order();point++) {
	//	value += a*dgData(cell,point)*dLagrange(k,order, xj[point])*wi[point];
	value += a*dgData(cell,point)*dLagrangeMatrix(k,point)*wi[point];
	//	std::cerr << k << "\t" << point << ":\t" << dLagrange(k,order, xj[point]) << "\t" << dLagrangeMatrix(k,point) << std::endl;
      }
      
      if(k==0)
	rhs(cell, k) = (value + fin[cell])/dx*2/wi[k];	
      else if(k==order-1)
	rhs(cell, k) = (value - fout[cell])/dx*2/wi[k];
      else
	rhs(cell, k) = (value)/dx*2/wi[k];
    }
    //    exit(0);
  }
}


#endif
