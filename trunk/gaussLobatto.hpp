#ifndef gaussLobatto_hpp
#define gaussLobatto_hpp

#include <BasisKlassen/array.hpp>
#include <BasisKlassen/vector.hpp>

extern const BK::Array<BK::Vector<double>,4> points;
extern const BK::Array<BK::Vector<double>,4> weights;

#endif
