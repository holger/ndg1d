#ifndef rk_hpp
#define rk_hpp

template<class DgData>
void singleStepEuler(DgData const& coeff, DgData& coeffNew, double dt, double dx, double a) {
  DgData rhs(coeff.size(), coeff.lx());
  
  dg(coeff, rhs, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffNew(i,order) = coeff(i,order) + dt*rhs(i,order);
  
  periodic(coeffNew);
}

template<class DgData>
void singleStepRk2(DgData const& coeff, DgData& coeffNew, double dt, double dx, double a) {
  DgData rhs(coeff.size(), coeff.lx());
  DgData coeffTemp(coeff.size(), coeff.lx());
  
  dg(coeff, rhs, dt, dx, a);
  
  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + 0.5*dt*rhs(i,order);

  // for(int i=1;i<coeff.size()-1;i++)  
  //   std::cout << coeffTemp(0,i) << "\t" << coeffTemp(1,i) << std::endl;
  
  periodic(coeffTemp);
  
  dg(coeffTemp, rhs, dt, dx, a);
  
  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffNew(i,order) = coeff(i,order) + dt*rhs(i,order);
  
  periodic(coeffNew);
}

template<class DgData>
void singleStepRk3(DgData const& coeff, DgData& coeffNew, double dt, double dx, double a) {
  DgData rhs(coeff.size(), coeff.lx());
  DgData coeff0(coeff.size(), coeff.lx());
  DgData coeff1(coeff.size(), coeff.lx());
  
  dg(coeff, rhs, dt, dx, a);
  
  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeff0(i,order) = coeff(i,order) + dt*rhs(i,order);

  // for(int i=1;i<coeff.size()-1;i++)  
  //   std::cout << coeffTemp(0,i) << "\t" << coeffTemp(1,i) << std::endl;
  
  periodic(coeff0);
  
  dg(coeff0, rhs, dt, dx, a);
  
  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeff1(i,order) = 0.75*coeff(i,order) + 0.25*coeff0(i,order) + 0.25*dt*rhs(i,order);
  
  periodic(coeff1);
  
  dg(coeff1, rhs, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffNew(i,order) = coeff(i,order)/3. + 2*coeff1(i,order)/3. + 2*dt*rhs(i,order)/3.;
  
  periodic(coeffNew);
}

template<class DgData>
void singleStepRk4(DgData const& coeff, DgData& coeffNew, double dt, double dx, double a) {
  DgData rhs0(coeff.size(), coeff.lx());
  DgData rhs1(coeff.size(), coeff.lx());
  DgData rhs2(coeff.size(), coeff.lx());
  DgData rhs3(coeff.size(), coeff.lx());

  DgData coeffTemp(coeff.size(), coeff.lx());

  // DgData coeff0(coeff.size(), coeff.lx());
  // DgData coeff1(coeff.size(), coeff.lx());
  // DgData coeff2(coeff.size(), coeff.lx());
  // DgData coeff3(coeff.size(), coeff.lx());
  
  dg(coeff, rhs0, dt, dx, a);
  
  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + 0.5*dt*rhs0(i,order);
  
  periodic(coeffTemp);
  //  applyLimiter(coeffTemp);
  dg(coeffTemp, rhs1, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + 0.5*dt*rhs1(i,order);
  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs2, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + dt*rhs2(i,order);
  
  periodic(coeffTemp);
  //  applyLimiter(coeffTemp);

  dg(coeffTemp, rhs3, dt, dx, a);
  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffNew(i,order) = coeff(i,order) + dt*(rhs0(i,order) + 2*rhs1(i,order) + 2*rhs2(i,order) + rhs3(i,order))/6.;
  
  periodic(coeffNew);
  //  applyLimiter(coeffNew);
}

template<class DgData>
void singleStepRk5(DgData const& coeff, DgData& coeffNew, double dt, double dx, double a) {
  DgData rhs0(coeff.size(), coeff.lx());
  DgData rhs1(coeff.size(), coeff.lx());
  DgData rhs2(coeff.size(), coeff.lx());
  DgData rhs3(coeff.size(), coeff.lx());
  DgData rhs4(coeff.size(), coeff.lx());
  DgData rhs5(coeff.size(), coeff.lx());

  DgData coeffTemp(coeff.size(), coeff.lx());

  dg(coeff, rhs0, dt, dx, a);
  
  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + 0.25*dt*rhs0(i,order);

  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs1, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + dt/8*(rhs0(i,order) + rhs1(i,order));
  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs2, dt, dx, a);


  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + dt*(-0.5*rhs1(i,order) + rhs2(i,order));
  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs3, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + dt*(3./16*rhs0(i,order) + 9./16*rhs3(i,order));
  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs4, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + dt*(-3./7*rhs0(i,order) + 2./7*rhs1(i,order) + 12./7*rhs2(i,order) - 12./7*rhs3(i,order) + 8./7*rhs4(i,order));
  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs5, dt, dx, a);
  
  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffNew(i,order) = coeff(i,order) + dt*(7*rhs0(i,order) + 32*rhs2(i,order) + 12*rhs3(i,order) + 32*rhs4(i,order) + 7*rhs5(i,order))/90.;
  
  periodic(coeffNew);
  // applyLimiter(coeffNew);
}

// template<class DgData>
// void singleStepRk5(DgData const& coeff, DgData& coeffNew, double dt, double dx, double a) {
//   DgData rhs0(coeff.size(), coeff.lx());
//   DgData rhs1(coeff.size(), coeff.lx());
//   DgData rhs2(coeff.size(), coeff.lx());
//   DgData rhs3(coeff.size(), coeff.lx());
//   DgData rhs4(coeff.size(), coeff.lx());
//   DgData rhs5(coeff.size(), coeff.lx());

//   DgData coeffTemp(coeff.size(), coeff.lx());

//   dg(coeff, rhs0, dt, dx, a);
  
//   for(int order=0; order<coeff.order(); order++) 
//     for(int i=1;i<coeff.size()-1;i++) 
//       coeffTemp(i,order) = coeff(i,order) + 0.25*dt*rhs0(i,order);

  
//   periodic(coeffTemp);
//   applyLimiter(coeffTemp);
//   dg(coeffTemp, rhs1, dt, dx, a);

//   for(int order=0; order<coeff.order(); order++) 
//     for(int i=1;i<coeff.size()-1;i++) 
//       coeffTemp(i,order) = coeff(i,order) + dt/8*(rhs0(i,order) + rhs1(i,order));
  
//   periodic(coeffTemp);
//   applyLimiter(coeffTemp);
//   dg(coeffTemp, rhs2, dt, dx, a);


//   for(int order=0; order<coeff.order(); order++) 
//     for(int i=1;i<coeff.size()-1;i++) 
//       coeffTemp(i,order) = coeff(i,order) + 0.5*dt*rhs2(i,order);
  
//   periodic(coeffTemp);
//   applyLimiter(coeffTemp);
//   dg(coeffTemp, rhs3, dt, dx, a);

//   for(int order=0; order<coeff.order(); order++) 
//     for(int i=1;i<coeff.size()-1;i++) 
//       coeffTemp(i,order) = coeff(i,order) + dt*(3./16*rhs0(i,order) - 3./8*rhs1(i,order) + 3./8*rhs2(i,order) + 9./16*rhs3(i,order));
  
//   periodic(coeffTemp);
//   applyLimiter(coeffTemp);
//   dg(coeffTemp, rhs4, dt, dx, a);

//   for(int order=0; order<coeff.order(); order++) 
//     for(int i=1;i<coeff.size()-1;i++) 
//       coeffTemp(i,order) = coeff(i,order) + dt*(-3./7*rhs0(i,order) + 8./7*rhs1(i,order) + 6./7*rhs2(i,order) - 12./7*rhs3(i,order) + 8./7*rhs4(order,1));
  
//   periodic(coeffTemp);
//   applyLimiter(coeffTemp);
//   dg(coeffTemp, rhs5, dt, dx, a);
  
//   for(int order=0; order<coeff.order(); order++) 
//     for(int i=1;i<coeff.size()-1;i++) 
//       coeffNew(i,order) = coeff(i,order) + dt*(7*rhs0(i,order) + 32*rhs2(i,order) + 12*rhs3(i,order) + 32*rhs4(i,order) + 7*rhs5(i,order))/90.;
  
//   periodic(coeffNew);
//   applyLimiter(coeffNew);
// }

template<class DgData>
void singleStepRk6(DgData const& coeff, DgData& coeffNew, double dt, double dx, double a) {
  DgData rhs1(coeff.size(), coeff.lx());
  DgData rhs2(coeff.size(), coeff.lx());
  DgData rhs3(coeff.size(), coeff.lx());
  DgData rhs4(coeff.size(), coeff.lx());
  DgData rhs5(coeff.size(), coeff.lx());
  DgData rhs6(coeff.size(), coeff.lx());
  DgData rhs7(coeff.size(), coeff.lx());

  DgData coeffTemp(coeff.size(), coeff.lx());

  dg(coeff, rhs1, dt, dx, a);
  
  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + dt*rhs1(i,order);

  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs2, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + dt*(3*rhs1(i,order) + rhs2(i,order))/8;
  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs3, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + dt*(8*rhs1(i,order) + 2*rhs2(i,order) + 8*rhs3(i,order))/27;
  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs4, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + dt*(+ 3*(3*sqrt(21)-7)*rhs1(i,order)
						- 8*(7-sqrt(21))*rhs2(i,order)
						+ 48*(7-sqrt(21))*rhs3(i,order)
						- 3*(21 - sqrt(21))*rhs4(i,order))/392;
  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs5, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + dt*(- 5*(231+51*sqrt(21))*rhs1(i,order)
						- 40*(7+sqrt(21))*rhs2(i,order)
						- 320*sqrt(21)*rhs3(i,order)
						+ 3*(21+121*sqrt(21))*rhs4(i,order)
						+ 392*(6+sqrt(21))*rhs5(i,order))/1960;
  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs6, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffTemp(i,order) = coeff(i,order) + dt*(+ 15*(22+7*sqrt(21))*rhs1(i,order)
						+ 120*rhs2(i,order)
						+ 40*(7*sqrt(21)-5)*rhs3(i,order)
						- 63*(3*sqrt(21)-2)*rhs4(i,order)
						- 14*(9*sqrt(21)+49)*rhs5(i,order)
						+ 70*(7-sqrt(21))*rhs6(i,order))/180;
  
  periodic(coeffTemp);
  // applyLimiter(coeffTemp);
  dg(coeffTemp, rhs7, dt, dx, a);

  for(int order=0; order<coeff.order(); order++) 
    for(int i=1;i<coeff.size()-1;i++) 
      coeffNew(i,order) = coeff(i,order) + dt*(9*rhs1(i,order) + 64*rhs3(i,order) + 49*rhs5(i,order) + 49*rhs6(i,order) + 9*rhs7(i,order))/180.;
  
  periodic(coeffNew);
  // applyLimiter(coeffNew);
}

#endif
